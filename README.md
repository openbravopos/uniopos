# [uniOPOS](https://uniopos.com/)

**uniOPOS** it is a preconfigured installation package of uniCenta OPOS for Points of Sales (POS/ERP Open Source)

**uniOPOS** es un paquete de instalación preconfigurado de uniCenta OPOS para Puntos de Ventas (POS/ERP Open Source)

[![Image](https://1.bp.blogspot.com/-EtgGNMrJ40c/X5DYTiL-EkI/AAAAAAAAOBk/c7wX7AKuXu45QyJUtkYPVY1I6OSyYaZuQCLcBGAsYHQ/s0/uniopos.png)](https://www.maravento.com/)

## DATA SHEET

---

[![Image](https://1.bp.blogspot.com/-Y_vVfquMvAE/WsOHgH6kY1I/AAAAAAAAD6Q/PPbPjbEBHH4YJDrcU6tE0ENbhHMroAmRQCLcBGAs/s1600/quick-download.png)](https://mega.nz/file/WI93ESza#kH--HTHii4NPyeHLoVUcO3mKJHgOlhviSQv3l4NJbxc)

|File|OS|Size|
| :---: | :---: | :---: |
|uniOPOS.exe (.zip)|Windows 10 x86 x64|1,72 GB|

## HOW TO USE

---

Disable your Antivirus, Antimalware, SmartScreen or any other security solution in your Operating System, close all windows and check the date and time of your PC is correct. Download uniOPOS.exe (.zip), unzip it to your desktop, execute it with double click (accept privileged execution) and follow the instructions on the screen

Desactive su Antivirus, Antimalware, SmartScreen o cualquier otra solución de seguridad en su Sistema Operativo, cierre todas las ventanas y verifique la fecha y hora de su PC sea la correcta. Descargue uniOPOS.exe (.zip), descomprimirlo en el escritorio, ejecutarlo con doble clic (acepte la ejecución con privilegios) y siga las instrucciones en pantalla

### IMPORTANT BEFORE USE

- Backward compatibility to Windows 10 is not guaranteed / La compatibilidad con versiones anteriores a Windows 10 no está garantizada
- If you have previous versions of uniOPOS (or some of its components: UnicentaOPOS, MariaDB, MySQL, etc.) installed on your system, it is highly recommended that you remove or stop the related services. uniOPOS can also remove previous versions of its components, so Backup before using it / Si tiene versiones previas de uniOPOS (o de algunos de sus componentes: UnicentaOPOS, MariaDB, MySQL, etc), instaladas en su sistema, se recomenda encarecidamente que las elimine o detenga los servicios relacionados. uniOPOS también puede eliminar versiones previas de sus componentes, por tanto haga Backup antes de usarlo
- According to the developers, [uniCenta oPOS v4x only uses MySQL and v5.7.x is recommended](https://unicenta.com/pages/configure-unicenta-opos/) / Según los desarrolladores, [uniCenta oPOS v4x solamente utiliza MySQL y v5.7.x es la recomendada](https://unicenta.com/pages/configure-unicenta-opos/)

[![Image](https://1.bp.blogspot.com/-1ZSYHM2rKI4/X1aPCtl_OFI/AAAAAAAAM60/ca7wRlr9GFcxZ5Wx5lAbEnCxRFekXZrYwCLcBGAsYHQ/s933/unicenta4.6.2.png)](https://www.maravento.com/)

- Always check the port in the UnicentaOPOS Control Panel (example: `jdbc:mysql://localhost:3306/`) that matches the port in use of MariaDB/MySQL / Verifique siempre el puerto en el Panel de Control de UnicentaOPOS (ejemplo: `jdbc:mysql://localhost:3306/`) que coincida con el puerto en uso de MariaDB/MySQL
- If you are going to work with MariaDB instead of MySQL, edit your `.sql` file and replace the line `ROW_FORMAT=COMPACT;` by `ROW_FORMAT=DYNAMIC;` / Si va a trabajar con MariaDB en lugar de MySQL, edite su archivo `.sql` y reemplace la línea `ROW_FORMAT=COMPACT;` por `ROW_FORMAT=DYNAMIC;`
- UniOPOS only contains three (3) installation packages (no-install packages .zip and wampserver). Select the one you will work with (note: UniOPOS does not (yet) include .msi installation packages for MySQL/MariaDB) / UniOPOS solo contiene tres (3) paquetes de instalación (paquetes sin instalación .zip y wampserver). Seleccione con el que va a trabajar. (nota: UniOPOS no incluye (aún) paquetes de instalación .msi para MySQL/MariaDB)
- At the end of the installation of each package the following message will appear / Al finalizar la instalación de cada paquete saldrá el siguiente mensaje

[![Image](https://1.bp.blogspot.com/-zXgEY8YK9NY/XmFlYI6dziI/AAAAAAAALJY/NEogpYmRevMSc8tfb6SJyyyiPW71zmo4gCLcBGAsYHQ/s1600/uniopos-end.png)](https://www.maravento.com)

### Unicenta OPOS + MySQL + Workbench

[![Image](https://1.bp.blogspot.com/-oQwYjqnGuTg/XmFlZEokWwI/AAAAAAAALJk/324KSU9_03YfFBMijOQbZC1o3IZjbaxxACLcBGAsYHQ/s1600/uniopos-mysql.png)](https://www.maravento.com)

#### Content

- [uniCenta OPOS 4.6.2 (.exe)](https://sourceforge.net/projects/unicentaopos/files/releases/windows/)
- [MySQL 5.7.32 x86 x64 (no-install package .zip)](https://dev.mysql.com/downloads/mysql/5.7.html#downloads/)
- [MySQL Workbench Community 8.0.22 (.msi)](https://dev.mysql.com/downloads/workbench/)

#### Important Before Use Unicenta OPOS + MySQL + WorkBench

- If you select the Unicenta OPOS package with MySQL + WorkBench, the path of MySQL is `%HOMEDRIVE%\mysql\mysql-%version%\data` folder / Si selecciona el paquete Unicenta OPOS con MySQL + WorkBench, el path de MySQL es `%HOMEDRIVE%\mysql\mysql-%version%\data`
- Installation is done in **insecure mode**, so access to `root` account is **without a password** (you must create one) / La instalacion se realiza en **modo inseguro**, por tanto el acceso a la cuenta `root` es **sin contraseña** (deberá crear una)

### Unicenta OPOS + MariaDB + DBeaver

[![Image](https://1.bp.blogspot.com/-hH1e_SqkB3M/XmFlYMz_wWI/AAAAAAAALJU/4QKwbj6NgNkFULi5GyduJxjoYWs13KrDACLcBGAsYHQ/s1600/uniopos-mariadb.png)](https://www.maravento.com)

#### Content

- [uniCenta OPOS 4.6.2 (.exe)](https://sourceforge.net/projects/unicentaopos/files/releases/windows/)
- [MariaDB 10.5.8 Stable x86 x64 (no-install package .zip)](https://downloads.mariadb.org/mariadb/)
- [DBeaver ce 7.3.0 x86 64 (.exe)](https://github.com/dbeaver/dbeaver/releases)

#### Important Before Use Unicenta OPOS + MariaDB + DBeaver

- If you select the Unicenta OPOS package with MariaDB + DBeaver, the path of MariaDB is `%HOMEDRIVE%\mariadb\mariadb-%version%\data` folder / Si selecciona el paquete Unicenta OPOS con MariaDB + dbeaver, el path de MariaDB es `%HOMEDRIVE%\mariadb\mariadb-%version%\data`
- According to developers, [MariaDB disables anonymous user by default](https://mariadb.com/kb/en/installing-mariadb-windows-zip-packages/), therefore installation is done in **safe mode** and access to the `root` account is with the password `secret` / Según los desarrolladores, [MariaDB desactiva el usuario anónimo de forma predeterminada](https://mariadb.com/kb/en/installing-mariadb-windows-zip-packages/), por tanto la instalacion se realiza en **modo seguro** y el acceso a la cuenta `root` es con la contraseña `secret`

### Unicenta OPOS + WampServer

[![Image](https://1.bp.blogspot.com/-flUtEE_bBVQ/XmFlZ66vFkI/AAAAAAAALJw/9F_WmXE0llc8lnb4alq69NJ-HkrAb1HoACLcBGAsYHQ/s1600/uniopos-wamp.png)](https://www.maravento.com)

#### Content

- [uniCenta OPOS 4.6.2 (.exe)](https://sourceforge.net/projects/unicentaopos/files/releases/windows/)
- [Wampserver 3.2.3 x86 x64 (.exe) with content: Apache 2.4.46 - PHP 5.6.40/7.3.21/7.4.9 - MySQL 5.7.31(default)|8.0.21 - MariaDB 10.3.23|10.4.13|10.5.4](http://wampserver.aviatechno.net/?lang=en)

#### Important Before Use Unicenta OPOS + WampServer

- If you select the Unicenta OPOS package + WampServer, keep in mind that installs MySQL (By default service start automatically) and MariaDB (By default service does not start automatically) / Si selecciona el paquete Unicenta OPOS + WampServer, tenga en cuenta que instala MySQL (Por defecto inicia automáticamente) y MariaDB (por defecto el servicio no inicia automáticamente)
- Installation is done in **insecure mode**, so access to `root` account is **without a password** (you must create one) / La instalacion se realiza en **modo inseguro**, por tanto el acceso a la cuenta `root` es **sin contraseña** (deberá crear una)
- Unicenta OPOS package + WampServer uses port 80 by default for Apache/phpmyadmin, therefore it is recommended to release this port (Note: if you use the IIS World Wide Web Publishing service, installing this package will change the IIS service to " manual ", to avoid conflicts) / Unicenta OPOS package + WampServer usa el puerto 80 por defecto para Apache/phpmyadmin, por tanto se recomienda liberar este puerto (Nota: si usa el servicio de IIS World Wide Web Publishing, la instalación de este paquete cambiará el servicio IIS a "manual", para evitar conflictos).

### Unicenta OPOS + UniServer Portable

[![Image](https://1.bp.blogspot.com/-A5v-uFxFy7g/X5DYfSwbW6I/AAAAAAAAOBo/OHDA8Bp-aeEiUAabgSdQKpvSwfJBJQYhACLcBGAsYHQ/s320/unicenta-uniserver.png)](https://www.maravento.com)

#### Content

- [uniCenta OPOS 4.6.2 (.exe)](https://sourceforge.net/projects/unicentaopos/files/releases/windows/)
- [Uniform Server Portable 14_0_2_ZeroXIV (apache_2_4_43, php_7_4_5, phpmyadmin_5_0_2)](https://sourceforge.net/projects/miniserver/) and module [ZeroXIV_mysql_5_7_28](https://sourceforge.net/projects/miniserver/files/Uniform%20Server%20ZeroXIV/ZeroXIV%20Modules/)

[![Image](https://1.bp.blogspot.com/-iG6SCUIc4NM/X5DYfeyqDcI/AAAAAAAAOBs/ktVmJzt4KpcIXFW4CwBxsQ3TooyRQCtrgCLcBGAsYHQ/s320/uniserverzero.png)](https://sourceforge.net/projects/miniserver/)

#### Important Before Use Unicenta OPOS + UniServer Portable

- If you select the Unicenta OPOS package + UniServer Portable, at the end of the installation, you must run UniController with administrative privileges / Si selecciona el paquete Unicenta OPOS + WampServer, al terminar la instalación, debe ejecutar UniController con privilegios administrativos
- for access to MySQL, user 'root' password 'root' (without quotes) / para el acceso a MySQL, usuario 'root' clave 'root' (sin comillas)

## DEPENDENCIES

---

- [WinExternal](https://github.com/maravento/winexternal)
- [trek](https://gitlab.com/maravento/trek)

## CONTRIBUTIONS

---

We thank all those who contributed to this project / Agradecemos a todos los que han contribuido con este proyecto

## LICENCES

---

[![GPL-3.0](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl.txt)

[![CreativeCommons](https://licensebuttons.net/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
[maravento.com](https://www.maravento.com) is licensed under a [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional License](http://creativecommons.org/licenses/by-sa/4.0/).

[WinZenity](https://github.com/maravento/winzenity), [Jpsoft](https://jpsoft.com/), [SteelWerX](https://fstaal01.home.xs4all.nl/swreg-us.html), [Microsoft](https://www.microsoft.com/), [74cz](http://74.cz/es/make-sfx/index.php), [Resource Hacker](http://www.angusj.com/resourcehacker/), [7zSFX Builder](https://sourceforge.net/projects/s-zipsfxbuilder/)

Unicenta OPOS ([Community Version](https://ftp://197.155.77.8/sourceforge/u/un/unicentaopos/releases/windows/) and [Official version](https://unicenta.com/download-files/installers/)), [Wampserver](http://wampserver.aviatechno.net/?lang=en), [MySQL](https://dev.mysql.com/downloads/mysql/5.7.html#downloads/), [MariaDB](https://downloads.mariadb.org/mariadb/), [dbeaver](https://github.com/dbeaver/dbeaver/releases), [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)

© 2021 [Maravento Studio](https://www.maravento.com), [Uniopos](https://uniopos.com/)

## DISCLAIMER

---

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
